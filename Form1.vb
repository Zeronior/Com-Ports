﻿Imports System.Management
Imports System.IO.Ports

Public Class Form1

 Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
  Me.Icon = My.Resources.disconnect
  AktualisierenToolStripMenuItem.Image = My.Resources.arrow_refresh
  BeendenToolStripMenuItem.Image = My.Resources.door_out

  GetSerial()
 End Sub

 Public Sub GetSerial()
        Try
            Dim comobjects As New List(Of String)
            Dim searcher As New ManagementObjectSearcher( _
                "root\CIMV2", _
                "SELECT * FROM Win32_SerialPort")

            For Each queryObj As ManagementObject In searcher.Get()
                ListBox1.Items.Add(queryObj("Caption") & ": " & queryObj("Status").ToString)
            Next
        Catch err As ManagementException
            MessageBox.Show("Es ist ein Fehler aufgetreten: " & err.Message)
        End Try
 End Sub

 Private Sub AktualisierenToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AktualisierenToolStripMenuItem.Click
        ListBox1.Items.Clear()
  GetSerial()
 End Sub

 Private Sub BeendenToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles BeendenToolStripMenuItem.Click
  End
 End Sub

    Private Sub ListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBox1.SelectedIndexChanged

    End Sub
End Class
