# Com-Ports

Windows-Tool um belegte Com-Ports mit Namen des Gerätes anzuzeigen. Zuletzt kompiliert auf Windows 10 mit VS 2022 Community, .Net Framework 7.

Verwendet die famfamfam silk Icons: ~~http://www.famfamfam.com/lab/icons/silk/~~

Leider sind die "silk Icons" online nicht mehr erreichbar. Bei Interesse sind sie noch bei Archive.org zu finden: https://web.archive.org/web/20220114141027/http://www.famfamfam.com/lab/icons/silk/
________________
A Windows based tool to show used com ports and their name. Last compiled on Windows 10 with VS 2022 Community, .Net Framework 7.

Uses famfamfam silk icons: ~~http://www.famfamfam.com/lab/icons/silk/~~

Unfortunately, the "silk icons" are no longer available online. If you are interested, you can still find them at Archive.org: https://web.archive.org/web/20220114141027/http://www.famfamfam.com/lab/icons/silk/
________________
Un outil basé sur Windows pour montrer les ports com utilisés et leur nom. Compilé pour la dernière fois sur Windows 10 avec VS 2022 Community, .Net Framework 7.

Utilise les icônes de soie famfamfam : ~~http://www.famfamfam.com/lab/icons/silk/~~

Malheureusement, les "silk icons" ne sont plus disponibles en ligne. Si vous êtes intéressé, vous pouvez toujours les trouver sur Archive.org : https://web.archive.org/web/20220114141027/http://www.famfamfam.com/lab/icons/silk/
